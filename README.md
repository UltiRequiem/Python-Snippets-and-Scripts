# Python Snippets and Scripts: Pythonadas
[![Maintenance](https://img.shields.io/badge/Maintained%3F-Yes-green.svg)](https://github.com/EliazBobadilla/Python-UltiSnippets-VSCode-Extension/commits/master) [![Contribute](https://img.shields.io/badge/Help-Contribute-551A8B.svg)](https://github.com/EliazBobadilla/Python-Snippets/blob/main/CONTRIBUTING.md) [![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://opensource.org) 

In this repository I will be leaving some other scripts that I have nowhere to put and occasionally some Snnipets.

If you want to install these snippets, at the moment you can only install them in:

- [VSCode](https://marketplace.visualstudio.com/items?itemName=EliazBobadilla.python-ultisnippets) 

I plan to make extensions for VIM and Notepad++ in future.
