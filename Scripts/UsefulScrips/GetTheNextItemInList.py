def main():
    current_position = 0    
    my_li = [1, 2, 3, 4, 5,6,7,8,9,10]    

    for i in range(0, 8):    
        value = my_li[current_position % (len(my_li))]    
        current_position += 1        
        print(value)

    print('---------------------')   

    for i in range(0, 5):    
        value = my_li[current_position % (len(my_li))]    
        current_position += 1        
        print(value) 

if __name__ == '__main__':
    main()