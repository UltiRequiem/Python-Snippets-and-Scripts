def main():
    dict_1 = {1: 'a', 2: 'b', 3: 'c'}
    dict_2 = {4: 'd', 5: 'e', 6: 'f'}
    print(dict_1) 
    print(dict_2) 
    dict_1.update(dict_2)
    print(dict_2)
    print(dict_1)
    
if __name__ == '__main__':
    main()