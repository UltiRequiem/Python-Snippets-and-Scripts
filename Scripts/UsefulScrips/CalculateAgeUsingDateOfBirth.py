from datetime import date

def calculate_age(born):
    today = date.today()
    try: 
        birthday = born.replace(year=today.year)
    except ValueError: 
        birthday = born.replace(year=today.year, day=born.day-1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year


if __name__ == "__main__":
    day, month, year = [int(x) for x in "13/10/2006".split("/")]
    born = date(year, month, day)
    print(calculate_age(born))
    
# https://www.programiz.com/python-programming/datetime/current-datetime