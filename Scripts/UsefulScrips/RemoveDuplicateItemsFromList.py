def main():
    myList = [1, 2, 3, 3, 2, 2, 4, 5, 5]
    print('First: ' +str(myList))
    
    myList = list(set(myList))
    print('Later: '+str(myList))
    
if __name__ == '__main__':
    main()