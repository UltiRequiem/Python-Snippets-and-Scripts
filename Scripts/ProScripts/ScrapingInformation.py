from bs4 import BeautifulSoup
import urllib2

def main():
    wiki = "http://sudo.utero.pe/2014/06/07/universidades-peru-intro/"
    header = {'User-Agent': 'Mozilla/5.0'}
    req = urllib2.Request(wiki,headers=header)
    page = urllib2.urlopen(req)
    soup = BeautifulSoup(page)
    
    area = ""
    district = ""
    town = ""
    county = ""
    
    table = soup.find("table", { "class" : "aligncenter" })
    f = open('universities.csv', 'w')
    
    for row in table.findAll("tr"):
        cells = row.findAll("td")
        if len(cells) == 4:
            universidad = cells[0].find(text=True)
            tipo = cells[1].findAll(text=True)
            lugar = cells[2].find(text=True)
            region = cells[3].find(text=True)
        print("----------------------------------------------------------------------------")
        print(universidad) 
        print(lugar)
        print(region)

        write_to_file = universidad + ", " + lugar + ", " + region + "\n"
        f.write(write_to_file.encode('utf-8'))
    
    f.close()

if __name__ == '__main__':
    main()